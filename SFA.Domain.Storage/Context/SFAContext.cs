﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFA.Domain.Storage.Context
{
    public class SFAContext:DbContext
    {
        public SFAContext():base(DomainConstants.Connectionstring)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SFAContext, Migrations.Configuration>(DomainConstants.Connectionstring));
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
