﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SalesForceAutomation.Startup))]
namespace SalesForceAutomation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
